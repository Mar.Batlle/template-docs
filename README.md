# GEL Help site for "your-app"

This repo contains the end user facing documentation for "your-app". 

More info:

* [SOP-EXT-014 How to Create and Maintain GEL Help websites](https://cnfl.extge.co.uk/display/GEQMS/SOP-EXT-014+How+to+Create+and+Maintain+GEL+Help+websites).
* Check out our [blog post](https://www.genomicsengland.co.uk/blog/improving-user-documentation) to find out about how we create our documentation.


